Name: Jon Raphael Apostol
Student ID#: 32302252

I, Jon Raphael Apostol, hereby certify that the files I submitted represent my
own work, that I did not copy any code from any other person or source, and that I did
not share our code with any other students.

I do not own the music included in the raw files. The music under the Main Activity is owned
by Taito Corporation, a subsidiary of Square Enix Holdings, Co. Limited. The music under the Game
activity is from this website: http://frogbobble.ytmnd.com/.
