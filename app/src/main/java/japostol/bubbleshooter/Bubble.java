package japostol.bubbleshooter;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import java.lang.Math;


/**
 * Created by Jon Apostol on 4/1/2015.
 */


public class Bubble {

    //COLOR OF BUBBLE
    private int color;

    //PAINT
    Paint paint = new Paint();

    //DETERMINES IF BUBBLE EXISTS
    private boolean exists;
    private int alpha;

    //LOCATION OF BUBBLE
    private int xCord;
    private int yCord;

    //SPEED OF BUBBLE
    private int xVel;
    private int yVel;

    //MAXIMUM
    private int xMax;
    private int yMax;

    private int radius;

    //STATES
    private boolean collide = false;
    private boolean move = false;
    //DETERMINES IF LEFT OR RIGHT
    private boolean side = false;
    private boolean leftSide = false;
    private boolean rightSide = false;

/***************************************CONSTRUCTORS*******************************************************/

    public Bubble(int color, int r, boolean exists){

        this.color = color;
        radius = r;
        this.exists = exists;

        //ASSIGNS A COLOR
        switch(color){
            case 0:
                this.color = 0;
                break;
            //RED
            case 1:
                this.color = Color.RED;
                break;
            //YELLOW
            case 2:
                this.color = Color.YELLOW;
                break;
            //GREEN
            case 3:
                this.color = Color.GREEN;
                break;
            //BLUE
            case 4:
                this.color = Color.BLUE;
                break;
            //CYAN
            case 5:
                this.color = Color.CYAN;
                break;
            //BLACK
            case 6:
                this.color = Color.BLACK;
                break;
            //MAGENTA
            case 7:
                this.color = Color.MAGENTA;
                break;
            //GREY
            case 8:
                this.color = Color.GRAY;
                break;
        }

        if(exists)
            alpha = 255;
        else
            alpha = 0;
    }

    /***************************************METHODS*******************************************************/
    /*
    ------------LIST OF FUNCTIONS------------
    public void setX(int x)
    public void setY(int y)

    public int getX()
    public int getY()

    public int getRadius()
    public int getColor()


    public void setColor(int color);

    public void setVelX(int x)
    public void setVelY(int y)

    public void setMax(int x, int y)

    public void setCoordinates()

    public void setMove(boolean x)
    public void isMove()
     */

    //SETS X AND Y COORDINATES
    public void setX(int x){
        xCord = x;
    }

    public void setY(int y){
        yCord = y;
    }

    //GETS X AND Y COORDINATES
    public int getX() { return xCord; }

    public int getY() { return yCord; }

    //GETS RADIUS
    public int getRadius(){
        return radius;
    }

    //RETURNS COLOR
    public int getColor(){
        return color;
    }

    //SETS COLOR IF CHANGED
    public void setColor(int color){
        this.color = color;
    }

    //SETS VELOCITY
    public void setVelX(int x)  { xVel = x; }

    public void setVelY(int y)  { yVel = y; }


    //SETS MAXIMUM
    public void setMax(int x, int y){
        xMax = x;
        yMax = y;
    }

    //PHYSICS
    public void setCoordinates(){
        if(move) {
            xCord += xVel;
            yCord += yVel;

            //X COORDINATES
            if (xCord > (xMax - radius)) {
                xVel = -xVel;
                xCord = xMax - radius;
            } else if (xCord < radius) {
                xVel = -xVel;
                xCord = radius;
            }

            //Y COORDINATES
            if (yCord > (yMax - radius)) {
                yVel = -yVel;
                yCord = xMax - radius;
            } else if (yCord < radius) {
                yVel = -yVel;
                yCord = radius;
            }
        }

    }

    //DETERMINES IF IT IS CLOSE TO BUBBLE
    public boolean collide(Bubble b){
        if(b.doesExists()) {
            int x = xCord + xVel;
            int y = yCord + yVel;

            int nextX = (int) Math.pow(b.getX() - x, 2);
            int nextY = (int) Math.pow(b.getY() - y, 2);

            int brad = (int) Math.sqrt(nextX + nextY);

            if(yCord >= b.getY() + 2*b.getRadius() && yCord <= b.getY() - 2*b.getRadius() ) {
                if(xCord <= b.getX()) {
                    leftSide = true;
                    rightSide = false;
                }
                else if(xCord > b.getX()){
                    leftSide = false;
                    rightSide = true;
                }
                side = true;
            }

            if (brad <= 2 * radius) {
                collide = true;
                exists = true;
                move = false;
                return true;
            }
        }

        return false;
    }

    public boolean isClose(Bubble b){
        int x = xCord + xVel;
        int y = yCord + yVel;

        int nextX = (int) Math.pow(b.getX() - x, 2);
        int nextY = (int) Math.pow(b.getY() - y, 2);

        int brad = (int) Math.sqrt(nextX + nextY);

        if(yCord >= b.getY() + 2*b.getRadius() && yCord <= b.getY() - 2*b.getRadius() ) {
            if(xCord <= b.getX()) {
                leftSide = true;
                rightSide = false;
            }
            else if(xCord > b.getX()){
                leftSide = false;
                rightSide = true;
            }
            side = true;
        }

        if (brad <= 2 * radius) {
            collide = true;
            exists = true;
            move = false;
            return true;
        }

        return false;
    }

    public boolean collided(){
        return collide;
    }

    public boolean atSide(){
        return side;
    }

    public boolean leftSide(){
        return leftSide;
    }

    public boolean rightSide()  {return rightSide;  }

    //DRAWS CIRCLE
    public void draw(Canvas c){
        paint.setColor(color);
        paint.setAlpha(alpha);
        c.drawCircle(xCord, yCord, radius, paint);
    }

    public void setMove(boolean x){
        move = x;
    }

    public boolean isMove(){    return move;    }

    public boolean doesExists(){    return exists;  }

    public void setExists(boolean x){
        exists = x;
        if(exists)
            alpha = 255;
        else {
            alpha = 0;
            color = 0;
        }
    }
}
