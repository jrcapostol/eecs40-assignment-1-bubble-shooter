package japostol.bubbleshooter;

import android.app.Activity;
import android.media.MediaPlayer;
import android.os.Bundle;



public class Game extends Activity {

    MediaPlayer playGameplay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //PLAYS MUSIC
        //BASED ON: http://stackoverflow.com/questions/5898722/mediaplayer-with-looped-audio-not-stopping-on-app-onpause
        playGameplay = MediaPlayer.create(Game.this, R.raw.gameplay);
        playGameplay.setLooping(true);
        playGameplay.start();


        int num = getIntent().getExtras().getInt("initBubble");
        setContentView(new BubbleShooterView(getBaseContext(), num));
    }

    //PAUSES SONG WHEN OUT OF ACTIVITY
    @Override
    protected void onPause(){
        super.onPause();
        playGameplay.pause();
    }

    //CONTINUES SONG WHEN BACK
    @Override
    protected void onResume(){
        super.onResume();
        playGameplay.start();
    }
}

