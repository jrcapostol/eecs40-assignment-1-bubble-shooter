package japostol.bubbleshooter;

import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;


public class MainActivity extends ActionBarActivity {
    //DETERMINES AMOUNT OF COLORS AND DIFFICULTY
    int num = 0;
    MediaPlayer playTheme;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //PLAYS MUSIC
        //BASED ON: http://stackoverflow.com/questions/5898722/mediaplayer-with-looped-audio-not-stopping-on-app-onpause
        playTheme = MediaPlayer.create(MainActivity.this, R.raw.theme);
        playTheme.setLooping(true);
        playTheme.start();
    }

    //PAUSES SONG WHEN OUT OF ACTIVITY
    @Override
    protected void onPause(){
        super.onPause();
        playTheme.pause();
    }

    //RESUMES SONG WHEN BACK
    @Override
    protected void onResume(){
        super.onResume();
        playTheme.start();
    }

    public void setEasy(View view){
        num = 4;

        //PASSES NUMBER TO GAME ACTIVITY
        Intent in = new Intent(MainActivity.this, Game.class);
        in.putExtra("initBubble", num);
        startActivity(in);
    }

    public void setMed(View view){
        num = 6;

        //PASSES NUMBER TO GAME ACTIVITY
        Intent in = new Intent(MainActivity.this, Game.class);
        in.putExtra("initBubble", num);
        startActivity(in);
    }

    public void setHard(View view){
        num = 8;

        //PASSES NUMBER TO GAME ACTIVITY
        Intent in = new Intent(MainActivity.this, Game.class);
        in.putExtra("initBubble", num);
        startActivity(in);
    }
}
