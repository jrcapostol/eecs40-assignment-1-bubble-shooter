package japostol.bubbleshooter;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.Toast;

import java.util.Random;
import java.util.Vector;

/**
 * Created by Jon Apostol on 3/31/2015.
 */

public class BubbleShooterView extends SurfaceView implements SurfaceHolder.Callback {

    private BubbleShooterThread bst;
    private Bubble[][] init = new Bubble[14][10];
    private Bubble bullet;
    private int color;

    private boolean crosshair = false;
    private float xCH;
    private float yCH;

    //DETERMINES LOCATION OF FINGERS
    private int locX = 0;
    private int locY = 0;

    //FOR TOAST
    Toast toast;
    Context con;
    CharSequence text;
    int duration = Toast.LENGTH_SHORT;

    //FOR DEBUG PURPOSES ONLY
    int dis1 = 0;
    int dis2 = 0;
    int recI;
    int recJ;
    boolean displayDebug = false;



    public BubbleShooterView(Context context, int init){
        super(context);
        con = context;
        color = init;
        //NOTIFY THE SURFACEHOLDER THAT YOU'D LIKE TO RECEIVE
        //SURFACEHOLDER CALLBACKS
        getHolder().addCallback(this);
        setFocusable(true);
        //INITIALIZE GAME STATE VARIABLES. DON'T RENDER THE GAME YET!
    }

    /*--------------------METHODS--------------------*/

    @Override
    public void surfaceCreated(SurfaceHolder holder){
        //Construct game initial state (bubbles, etc.)

        //Launch animator thread
        bst = new BubbleShooterThread(this);
        bst.start();

//        CharSequence text = Integer.toString(init);
//        int duration = Toast.LENGTH_LONG;
//
//        Toast toast = Toast.makeText(cont, text, duration);
//        toast.show();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height){
        //Respond to surface changes, e.g., aspect ratio changes.

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder){
        //The cleanest way to stop a thread is by interrupting it.
        //BubbleShooterThread regularly checks its interrupt flag.
        bst.interrupt();
    }

    @Override
    public void onDraw(Canvas c){
        super.onDraw(c);
        renderGame(c);
    }

    @Override
    public boolean onTouchEvent(MotionEvent e){
        //UPDATE GAME STATE IN RESPONSE TO EVENTS;
        // TOUCH-DOWN, TOUCH-UP, AND TOUCH-MOVE.
        //CURRENT FINGER POSITION

        int curX = (int)e.getX();
        int curY = (int) e.getY();

        switch(e.getAction()){
            case MotionEvent.ACTION_DOWN:
                //UPDATE GAME STATE.
                //SETS THE WIDTH AND HEIGHT
                bullet.setMax(getWidth(), getHeight());
            case MotionEvent.ACTION_MOVE:
                //UPDATE GAME STATE.
                if(!bullet.isMove()){
                    crosshair = true;
                    yCH = e.getY();
                    xCH = e.getX();
                }
                break;
            case MotionEvent.ACTION_UP:
                //UPDATE GAME STATE
                crosshair = false;
                if(!bullet.isMove()){
                    bullet.setVelY(-(getWidth() / 40));
                    bullet.setVelX((curX - bullet.getX()) / 25);

                    bullet.setMove(true);
                }
                break;
        }
        return true;
    }

    public void advanceFrame(Canvas c){
        //UPDATE GAME STATE TO ANIMATE MOVING OR EXPLODING BUBBLES
        // (E.G., ADVANCE LOCATION OF MOVING BUBBLE).


        renderGame(c);

        if(!bullet.collided()){
            bullet.setCoordinates();
        }


        if(bullet.isMove()) {
            for (int i = 0; i < 14; i++) {
                for (int j = 0; j < 10; j++) {
                        if (bullet.collide(init[i][j])) {
                            try {
                                if(bullet.leftSide()){
                                    init[i][j-1].setColor(bullet.getColor());
                                    init[i][j-1].setExists(true);
                                }
                                else if(bullet.rightSide()){
                                    init[i][j+1].setColor(bullet.getColor());
                                    init[i][j+1].setExists(true);
                                }
                                else {
                                    init[i + 1][j].setColor(bullet.getColor());
                                    init[i + 1][j].setExists(true);
                                }
                                checkDuplicate(i+1, j);
                                checkFloatingBubbles();
                                bullet = null;
                                displayDebug = true;
                                recI = i;
                                recJ = j;
                                initializeBullet();
                                break;
                            }
                            //RESTARTS GAME IF GAME OVER DUE TO OVERLOAD
                            catch(Exception e){
                                bullet = null;
                                //text = "You lose!";
                                //toast = Toast.makeText(con, text, duration);
                                //toast.show();
                                initializeGame();
                                initializeBullet();

                                break;
                            }

                    }
                    // IF THERE IS NO BUBBLE IN FIRST ROW
                    else if(!init[0][j].doesExists() && bullet.isClose(init[0][j])){
                            init[0][j].setColor(bullet.getColor());
                            init[0][j].setExists(true);
                            checkDuplicate(0, j);
                            checkFloatingBubbles();
                            bullet = null;
                            initializeBullet();
                        }
                }

            }
        }

        //STARTS GAME AGAIN IF ALL IS EMPTY
        if(isEmpty()){
            text = "You win!";
            toast = Toast.makeText(con, text, duration);
            toast.show();
            initializeGame();
            initializeBullet();
        }

    }

    private void checkDuplicate(int x, int y) {
        Vector<Bubble> checkBubbles = new Vector<Bubble>();
        checkBubbles.add(init[x][y]);

        try{

            if(x > 0) {
                //CHECKS ROW
                for (int i = x; i >= 0; i--) {

                    if (i < 0) break;

                    if (checkBubbles.elementAt(0).getColor() != init[i][y].getColor() && init[i][y].doesExists())
                        break;


                    if (checkBubbles.elementAt(0).getColor() == init[i][y].getColor() && init[i][y].doesExists()) {
                        checkBubbles.add(init[i][y]);
                        //CHECK LEFT HAND SIDE
                        for (int j = y - 1; j >= 0; j--) {
                            if (j < 0) break;
                            if (checkBubbles.elementAt(0).getColor() == init[i][j].getColor() && init[i][j].doesExists()) {
                                checkBubbles.add(init[i][j]);
                                if (j < y - 1) {
                                    //CHECKS ABOVE
                                    for (int k = i - 1; k >= 0; k--) {
                                        if (k < 0) break;
                                        if (checkBubbles.elementAt(0).getColor() == init[k][j].getColor()  && init[k][j].doesExists())
                                            checkBubbles.add(init[k][j]);
                                        else if (!init[k][j].doesExists())
                                            break;
                                    }


                                    //CHECKS BELOW
                                    for (int k = i + 1; k < 14; k++) {
                                        if (checkBubbles.elementAt(0).getColor() == init[k][j].getColor() && init[k][j].doesExists())
                                            checkBubbles.add(init[k][j]);
                                        else if (!init[k][j].doesExists())
                                            break;

                                    }
                                }
                            } else
                                break;
                        }

                        //CHECK RIGHT HAND SIDE
                        for (int j = y + 1; j < 10; j--) {
                            if (checkBubbles.elementAt(0).getColor() == init[i][j].getColor() && init[i][j].doesExists()) {
                                checkBubbles.add(init[i][j]);
                                if (j > y + 1) {
                                    //CHECKS ABOVE
                                    for (int k = i - 1; k >= 0; k--) {
                                        if (k < 0) break;
                                        if (checkBubbles.elementAt(0).getColor() == init[k][j].getColor() && init[k][j].doesExists())
                                            checkBubbles.add(init[k][j]);
                                        if(!init[k][j].doesExists())
                                            break;
                                    }

                                    //CHECKS BELOW
                                    for (int k = i + 1; k < 14; k++) {
                                        if (checkBubbles.elementAt(0).getColor() == init[k][j].getColor() && init[k][j].doesExists())
                                            checkBubbles.add(init[k][j]);
                                        else if (!init[k][j].doesExists())
                                            break;

                                    }
                                }
                            } else
                                break;
                        }
                    }

                }   /*end of for loop*/

            }

            //WHEN AT THE MOST TOP ROW
            else{
                //CHECK LEFT SIDE
                for(int i = y-1; i >= 0; i--){
                    if (checkBubbles.elementAt(0).getColor() == init[x][i].getColor() && init[x][i].doesExists())
                        checkBubbles.add(init[x][i]);
                }
                //CHECK RIGHT SIDE
                for(int i = y+1; i < 10; i++){
                    if (checkBubbles.elementAt(0).getColor() == init[x][i].getColor()&& init[x][i].doesExists())
                        checkBubbles.add(init[x][i]);
                }
            }

        } catch (Exception e){

        }

        if (checkBubbles.size() > 3) {
            for (int i = 0; i < checkBubbles.size(); i++) {
                checkBubbles.elementAt(i).setExists(false);
                checkBubbles.elementAt(i).setColor(0);
            }
        }
    }



    public void renderGame(Canvas c){

        //BACKGROUND
        Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.WHITE);
        paint.setAntiAlias(true);
        c.drawPaint(paint);

        //DISPLAYS TOP BUBBLES
        for(int i = 0; i < 14; i++){
            for(int j = 0; j < 10; j++){
                if(init[i][j] != null && init[i][j].getColor() != 0) {
                    init[i][j].draw(c);
                }
            }
        }

        //DISPLAYS BUTTOM BUBBLE
        bullet.draw(c);

        //DRAWS CROSSHAIR
        if(crosshair){
            Paint black = new Paint();
            black.setColor(Color.BLACK);
            //X
            c.drawLine(xCH, 0, xCH, getHeight(), black);
            //Y
            c.drawLine(0, yCH, getWidth(), yCH, black);

            //FROM BALL TO CROSSHAIR
            c.drawLine((float)bullet.getX(), (float)bullet.getY(), xCH, yCH, black);
        }

    }

    public void initializeGame(){
        Random rn = new Random();

        //BUBBLE BULLET
        initializeBullet();

        //INITIALIZES BUBBLES
        for(int i = 0; i < 14; i++){
            for(int j = 0; j < 10; j++){
                int setColor = rn.nextInt(color) + 1;

                if(i < 7) {
                        init[i][j] = new Bubble(setColor, getWidth() / 20, true);
                        init[i][j].setX(getWidth() / 20 + 2 * getWidth() / 20 * j);
                        init[i][j].setY(getHeight() / 20 + 2 *getWidth() / 20 * i);
                }
                else{
                        init[i][j] = new Bubble(0, getWidth() / 20, false);
                        init[i][j].setX(getWidth() / 20 + 2 * getWidth() / 20 * j);
                        init[i][j].setY(getHeight() / 20 + 2 *getWidth() / 20 * i);
                }
            }
        }

    }

    public void initializeBullet(){
        Random rn = new Random();

        bullet = new Bubble(rn.nextInt(color) + 1, getWidth()/20, true);
        bullet.setX(getWidth()/2);
        bullet.setY(getHeight() - 2*getWidth()/20);
    }

    private void checkFloatingBubbles(){
        //MAKES SURE THAT THERE IS NO FLOATING BUBBLES
        for(int i = 1; i < 14; i++){
            for(int j = 0; j < 10; j++){
                if(!init[i][j].doesExists())
                    continue;
                if(!init[i-1][j].doesExists()){
                    init[i][j].setExists(false);
                    init[i][j].setColor(0);
                }
            }
        }
    }

    private boolean isEmpty(){
        for(int i = 0; i < 14; i++){
            for(int j = 0; j < 10; j++){
                if(init[i][j].doesExists())
                    return false;
            }
        }
        return true;
    }

}

